import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class StartScreen extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JTextPane instructionText;

    public StartScreen(String taskInstruction, List<String> input, List<String> output) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        StringBuilder sb = new StringBuilder("Using the operation available on right-click, please create code for the following task: ");
        sb.append(taskInstruction);
        sb.append("\n\n");
        sb.append("Initial input/output examples:\n");
        for(int i = 0; i < input.size(); ++i) {
            sb.append(input.get(i));
            sb.append(" -> ");
            sb.append(output.get(i));
            sb.append("\n");
        }
        sb.append("\nIf you need to take a break, please do so only between program screens. " +
                "Feel free to use a Scala interpreter. Please do not use the internet.");

        instructionText.setText(sb.toString());
    }

    private void onOK() {
// add your code here
        dispose();
    }
}
