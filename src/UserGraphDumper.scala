import java.io._

import IterInfo._

import scala.collection.mutable

/**
 * Created by hila on 11/04/2017.
 */
object UserGraphDumper extends App{
  val files = args.flatMap{dir => new java.io.File(dir).listFiles(new FilenameFilter {
    override def accept(dir: File, name: String): Boolean = name.endsWith(".session")
  })}

  val sessions = files.map { f =>
    val stream = new ObjectInputStream(new FileInputStream(f))
    val res = mutable.MutableList[UserEvent]()
    try {
      while (true) {
        val line = stream.readObject().asInstanceOf[UserEvent]
        line match {
          case SessionStarted(exercise, initialExamples, runMode) => res += line
          case PredicatesAdded(iterationLength, predicatesSelected, initialClassSize) => res += line
          case ProgramAccepted(iterationLength, acceptedProgram, initialClassSize) => res += line
          case _ => {}
        }
      }
    }
    catch {
      case eof: EOFException => //this is fine
      case e: Exception => throw e
    }

    res.toList
  }

  val byExAndMode = sessions.groupBy{session => val start = session(0).asInstanceOf[SessionStarted]; (start.exercise.take(20), start.runMode)}
  byExAndMode.foreach { be =>
    println(be._1)
    val maxlen = be._2.map(_.length).max
    (1 until maxlen).foreach {i =>
      print(i)
      print("\t")
      print(
        be._2.map(session => if (session.length > i) session(i) match {
          case PredicatesAdded(iterationLength, predicatesSelected, initialClassSize) =>
            if (predicatesSelected.exists(_.isInstanceOf[OutputEqualsPredicate]) && predicatesSelected.exists(!_.isInstanceOf[OutputEqualsPredicate]))
              "0"
            else if (predicatesSelected.exists(_.isInstanceOf[OutputEqualsPredicate])) "-1"
            else "1"

          case ProgramAccepted(iterationLength, acceptedProgram, initialClassSize) => ""
        } else "").mkString("\t")
      )
      println()
    }
  }
//  byExAndMode.foreach{ be =>
//    println(be._1)
//    val maxlen = be._2.map(_.length).max
//    (1 until maxlen).foreach {i =>
//      print(i)
//      print("\t")
//      print(be._2.map(session => if (session.length > i) session(i) match {
//        case PredicatesAdded(iterationLength, predicatesSelected, initialClassSize) => {
//          val count: Int = predicatesSelected.count(p => p match {
//            case OutputEqualsPredicate(in, out) => true
//            case _ => false
//          })
//          if (count > 0) iterationLength / count else iterationLength
//        }
//        case ProgramAccepted(iterationLength, acceptedProgram, initialClassSize) => iterationLength
//        } else 0).mkString("\t"))
//      println()
//    }
//  }
}
