import javax.script.{ScriptEngine, ScriptEngineManager}

import scala.tools.nsc.interpreter.IMain

/**
 * Created by hila on 23/03/2017.
 */
object ScalaCompiler {
  def reset() = {
    engine.asInstanceOf[IMain].reset()
    makeEngine(engine)
  }

  var engine = {
    val engine = new ScriptEngineManager().getEngineByName("scala")
    makeEngine(engine)
    engine
  }

  def makeEngine(engine : ScriptEngine) : Unit = {
    val settings = engine.asInstanceOf[scala.tools.nsc.interpreter.IMain].settings
    val scalaJar = System.getProperty("java.class.path").split(java.io.File.pathSeparator)
      .filter {
        _.contains("scala-library")
      }.head
    settings.bootclasspath.append(scalaJar)
    settings.classpath.append(scalaJar)
    engine.eval("import scala.util.Try")
  }

  def setInputs(inputs : List[AnyRef]) = {
    engine.eval("val inputs = " + inputs.mkString("List(",",",")")).asInstanceOf[List[AnyRef]]
  }

}
