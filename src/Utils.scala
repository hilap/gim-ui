/**
 * Created by hila on 23/03/2017.
 */
object Utils {
  def escape(raw: String): String = {
    import scala.reflect.runtime.universe._
    Literal(Constant(raw)).toString
  }
  def prettyPrint(ref : Any) : String = ref match {
    case arr : Array[_] => arr.map(x => Utils.prettyPrint(x)).mkString("Array(",",",")")
    case lst : List[_] => lst.map(x => Utils.prettyPrint(x)).mkString("List(",",",")")
    case vec : Vector[_] => vec.map(x => Utils.prettyPrint(x)).mkString("List(",",",")")
    case map : Map[_,_] => map.map(kv => Utils.prettyPrint(kv._1) + " -> " + Utils.prettyPrint(kv._2)).mkString("Map(",",",")")
    case tup : (_,_) => (prettyPrint(tup._1),prettyPrint(tup._2)).toString()
    case str : String => escape(str)
    case _ => ref.toString
  }
}
