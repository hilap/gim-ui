import scala.tools.nsc.interpreter.IMain;

import javax.script.ScriptException;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;

public class InputOutputDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    public JTextField input;
    public JTextField output;
    private JLabel inputRes;
    private JLabel outputRes;

    private long startTime;

    public InputOutputDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        input.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                compile(input,inputRes);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                compile(input,inputRes);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                compile(input,inputRes);
            }
        });

        output.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                compile(output,outputRes);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                compile(output,outputRes);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                compile(output,outputRes);
            }
        });

        this.addWindowListener(new WindowAdapter() {
            public void windowDeactivated(WindowEvent e)
            {
                if (isOk) {
                    IterInfo.logExampleOk(System.currentTimeMillis() - startTime);
                }
                else {
                    IterInfo.logExampleCancel(System.currentTimeMillis() - startTime);
                }
            }
        });

        startTime = System.currentTimeMillis();
    }

    private void compile(JTextField textField, JLabel resultLbl) {
        if (textField.getText().isEmpty()) {
            textField.setBackground(Color.WHITE);
            resultLbl.setText("");
        }
        else {
            try {
                //((IMain)ScalaCompiler.engine()).reset();
                //Object eval = ScalaCompiler.engine().eval(textField.getText());
                ((IMain)ScalaCompiler.engine()).compile(textField.getText());
                //if (eval == null)
                //    throw new ScriptException("Result is null");
                //resultLbl.setText(Utils.prettyPrint(eval));
                textField.setBackground(Color.GREEN);
            } catch (ScriptException e) {
                textField.setBackground(Color.RED);
                resultLbl.setText("");
            }
        }
    }

    public boolean isOk = false;
    private void onOK() {
// add your code here
        //dispose();
        if (input.getBackground() == Color.green && output.getBackground() == Color.green) {
            isOk = true;
            this.setVisible(false);
        }
        else {
            JOptionPane.showMessageDialog(this, "Both expressions must compile to add example", "Can't add example", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void onCancel() {
// add your code here if necessary
        //dispose();
        this.setVisible(false);
    }
}
