import java.io._

import IterInfo._

import scala.collection.mutable

/**
 * Created by hila on 12/04/2017.
 */
object TraLaLa extends App{
  val files = args.flatMap{dir => new java.io.File(dir).listFiles(new FilenameFilter {
    override def accept(dir: File, name: String): Boolean = name.endsWith(".session")
  })}

  val sessions = files.map { f =>
    val stream = new ObjectInputStream(new FileInputStream(f))
    val res = mutable.MutableList[UserEvent]()
    try {
      while (true) {
        val line = stream.readObject().asInstanceOf[UserEvent]
        res += line
      }
    }
    catch {
      case eof: EOFException => //this is fine
      case e: Exception => throw e
    }

    res.toList
  }

  val byExAndMode = sessions.groupBy{session => val start = session(0).asInstanceOf[SessionStarted]; (start.exercise.take(20), start.runMode)}.filter(p => p._1._2 == "io")

  byExAndMode.foreach{ ex =>
    println(ex._1)
    ex._2.filter(session => session.last.isInstanceOf[ProgramAccepted]).foreach{ session =>
      println(session.flatMap(event => event match {
        case PredicatesAdded(iterationLength, predicatesSelected, initialClassSize) => Some(initialClassSize)
        case ProgramAccepted(iterationLength, acceptedProgram, initialClassSize) => Some(initialClassSize)
        case _ => None
      }).mkString("\t"))
    }
  }
}
