import java.io._

import IterInfo._

import scala.collection.mutable


/**
 * Created by hila on 10/04/2017.
 */
object InfoDumper extends App{
  val files = args.flatMap{dir => new java.io.File(dir).listFiles(new FilenameFilter {
    override def accept(dir: File, name: String): Boolean = name.endsWith(".session")
  })}

  val sessions = files.map { f =>
    val stream = new ObjectInputStream(new FileInputStream(f))
    val res = mutable.MutableList[UserEvent]()
    try {
      while (true) {
        val line = stream.readObject().asInstanceOf[UserEvent]
        res += line
      }
    }
    catch {
      case eof: EOFException => //this is fine
      case e: Exception => throw e
    }

    res.toList
  }

  val byExAndMode = sessions.groupBy{session => val start = session(0).asInstanceOf[SessionStarted]; (start.exercise.take(20), start.runMode)}
  byExAndMode.foreach {exs =>
    println(exs._1)
    exs._2.foreach{session =>
      //total time:
      val sessionTimes = session.flatMap{ event => event match {
        case ProgramAccepted(time, _,_) => Some(time)
        case PredicatesAdded(time,_,_) => Some(time)
        case _ => None
      }}
      print(sessionTimes.sum)
      print("\t")
      print(sessionTimes.sum.toDouble / sessionTimes.size)
      print("\t")
      val predicateTimes = session.flatMap{ event => event match {
        case PredicatesAdded(time,_,_) => Some(time)
        case _ => None
      }}
      print(predicateTimes.sum.toDouble / predicateTimes.size)
      print("\t")
      //num iters
      print(session.map{ event => event match {
        case ProgramAccepted(_, _,_) => 1
        case PredicatesAdded(_,_,_) => 1
        case _ => 0
      }}.sum)
      print("\t")
      val numPredicates = session.map{ event => event match {
        case PredicatesAdded(_, predicates, _) => predicates.size
        case _ => 0
      }}.sum
      print(numPredicates)
      print("\t")
      val numExamples = session.map{ event => event match {
        case PredicatesAdded(_,predicates,_) => predicates.map{ p => p match {
          case OutputEqualsPredicate(in, out) => 1
          case _ => 0
        }
        }.sum
        case _ => 0
      }}.sum
      print(numExamples)
      print("\t")
      val itersWithEx = session.filter{ event => event match {
        case PredicatesAdded(iterationLength, predicatesSelected, initialClassSize) =>
          predicatesSelected.exists(p => p match {
            case OutputEqualsPredicate(in, out) => true
            case _ => false
          })
        case _ => false
      } }.map(event => event.asInstanceOf[PredicatesAdded])
      print(itersWithEx.size)
      print("\t")
      print(itersWithEx.map(iter => iter.iterationLength).sum.toDouble / itersWithEx.size)
      println()
    }
  }
}
