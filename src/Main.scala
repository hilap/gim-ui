import java.awt.TrayIcon.MessageType
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import javax.swing.JOptionPane
import StepDisplayer.TestMode
import collection.JavaConversions._

/**
 * Created by hila on 20/03/2017.
 */
trait Predicate {
  def holds(program : String, execution: List[AnyRef]) : Boolean
}
case class PrefixPredicate(seq : Array[String]) extends Predicate {
  override def holds(program: String, execution: List[AnyRef]): Boolean = program == seq.mkString(".") || program.startsWith(seq.mkString(".") + ".")
  override def toString = "Set prefix: " + seq.mkString("[",",","]")
}
case class RetainPredicate(seq : Array[String]) extends Predicate {
  override def holds(program: String, execution: List[AnyRef]): Boolean = if (seq(0) == "input")
      program.startsWith(seq.mkString("."))
    else (program.endsWith("." + seq.mkString(".")) || program.contains("." + seq.mkString(".") + "."))
  override def toString = "Always include: " + seq.mkString("[",",","]")
}
case class ExcludePredicate(seq : Array[String]) extends Predicate {
  override def holds(program: String, execution: List[AnyRef]): Boolean = !(if (seq(0) == "input")
      program.startsWith(seq.mkString("."))
    else (program.endsWith("." + seq.mkString(".")) || program.contains("." + seq.mkString(".") + "."))
   )
  override def toString = "Never include: " + seq.mkString("[",",","]")
}

case class OutputEqualsPredicate(in: String, out : String) extends Predicate {
  lazy val outExecuted = ScalaCompiler.engine.eval(out)
  var idx : Int = -1
  def setIdx : Unit = {idx = OutputEqualsPredicate.nextIdx}
  override def holds(program: String, execution: List[AnyRef]): Boolean = execution(idx) == outExecuted
  override def toString = "For input " + in + " produce output " + out
}

object OutputEqualsPredicate {
  private var idx = 0
  def nextIdx : Int = {
    val res = idx
    idx = idx + 1
    res
  }
  def zeroIdx : Unit = {
    idx = 0
  }
}

//case class NotThisProgram(prog : String) extends Predicate {
//  override def holds(program: String, execution: List[AnyRef]): Boolean = program != prog
//}


object Main extends App{
  def loadInitialPrograms(in : List[String], out : List[String], progsFile : String) : (Programs,Programs) = {
    var programs = Programs(progsFile)
    //var outputs : List[AnyRef] = ScalaCompiler.engine.eval(out.mkString("List(",",",")")).asInstanceOf[List[AnyRef]]
    val (good,bad) = programs.filter(in.zip(out).map{p =>
      val pred = OutputEqualsPredicate(p._1, p._2)
      pred.setIdx
      pred
    })
    val prefixes = bad.keepPrefixes(good)
    programs = good
    (programs,prefixes)
  }

  def loadInputOutputs(inputOutputsFile : String) = scala.io.Source.fromFile(inputOutputsFile).getLines()
    .map(l => l.split("\t"))
    .foldLeft((List[String](),List[String]()))((z,f) => (z._1 :+ f(0),z._2 :+ f(1)))

  var (in,out) = loadInputOutputs(args(1))
  var inputs = ScalaCompiler.setInputs(in)

  var (programs,prefixes) = loadInitialPrograms(in,out,args(0))

  private val instructions: String = args(2)
  val s : StartScreen = new StartScreen(instructions,in,out)
  s.pack()
  s.setSize(s.getPreferredSize)
  s.setLocationRelativeTo(null)
  s.setVisible(true)

  val runMode = StepDisplayer.TestMode.valueOf(args(3))
  //create output file from dir
  val outDir = new File(args(4))
  if (!outDir.isDirectory) throw new IllegalArgumentException(args(4) + " is not a directory")
  val outFile : String =   new File(outDir,runMode + new SimpleDateFormat("yyyyMMddHHmm'.session'").format(new Date())).getPath()
  IterInfo.setFile(outFile)

  IterInfo.logSessionStarted(args(2),in.zip(out).mkString(","),runMode.name())

  while (true) {

    if (programs.isEmpty) {
      IterInfo.logProgramsEmptied()
      val decision = JOptionPane.showConfirmDialog(null, "No more programs left to show\nWould you like to start over?","",JOptionPane.YES_NO_OPTION);
      if (decision == JOptionPane.YES_OPTION) {
        OutputEqualsPredicate.zeroIdx
        val (i,o) = loadInputOutputs(args(1))
        in = i
        out = o
        inputs = ScalaCompiler.setInputs(in)
        val (progs,prefs) = loadInitialPrograms(in,out,args(0))
        programs = progs
        prefixes = prefs
      }
      else {//no option
        IterInfo.flushClose
        System.exit(0)
      }
    }
    val program = programs.get
    val splitProg = Programs.splitProgram(program._1)
    val execution = (1 to splitProg.length)
      .map(i => splitProg.take(i).mkString("."))
      .map(prog => if (programs.containsProgram(prog))
          programs.getResult(prog).map(Utils.prettyPrint).toArray //replace toString with prettyPrint
        else prefixes.getResult(prog).map(Utils.prettyPrint).toArray).toArray

    JOptionPane.showMessageDialog(null, "Get ready", "", JOptionPane.INFORMATION_MESSAGE)
    val startTime = System.currentTimeMillis();

    val step: StepDisplayer = new StepDisplayer(splitProg, execution, runMode,program._1,in.mkString("List(",",",")"),instructions)
    step.pack()
    //step.setSize(step.getPreferredSize)
    step.setLocationRelativeTo(null)
    step.setVisible(true)

    val iterTime = System.currentTimeMillis() - startTime

    if (step.finished) {
      IterInfo.logAcceptIter(iterTime,program._1,programs.programs.size)
      IterInfo.flushClose
      System.exit(0);
    }
    IterInfo.logPredicateIter(iterTime,step.predicates.toList,programs.programs.size)

    val (exs,notex) = step.predicates.toList.partition(_.isInstanceOf[OutputEqualsPredicate])
    val examples = exs.map(_.asInstanceOf[OutputEqualsPredicate])


    val (good,bad) = programs.filter(notex /*:+ NotThisProgram(program._1)*/)
    programs = good
    prefixes = prefixes.union(bad).keepPrefixes(programs)
    if (!examples.isEmpty) {
      examples.foreach(ex => ex.setIdx)
      in ++= examples.map(_.in)
      inputs = ScalaCompiler.setInputs(in)
      programs = programs.reExecute()
      val(good,bad) = programs.filter(examples)
      programs = good
      //also recalc prefixes
      prefixes = prefixes.union(bad).keepPrefixes(programs)
      prefixes = prefixes.reExecute()
    }

  }
  IterInfo.flushClose
  System.exit(0);
}
