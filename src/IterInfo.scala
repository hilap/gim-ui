import java.io.{FileOutputStream, ObjectOutputStream, OutputStream}
import java.util

import com.sun.corba.se.impl.orbutil.ObjectWriter

/**
 * Created by hila on 28/03/2017.
 */
object IterInfo {
  trait UserEvent {}

  private var writer = new ObjectOutputStream(new OutputStream {
    override def write(b: Int): Unit = {}
  }) //null writer

  def setFile(filename : String) = { writer = new ObjectOutputStream(new FileOutputStream(filename)) }
  def flushClose = {
    writer.flush()
    writer.close()
  }

  case class SessionStarted(exercise: String, initialExamples : String, runMode : String) extends UserEvent
  def logSessionStarted(exercise: String, initialExamples : String, runMode : String) = writer.writeObject(
    SessionStarted(exercise,initialExamples, runMode))

  case object ProgramsEmptied extends  UserEvent
  def logProgramsEmptied() = writer.writeObject(ProgramsEmptied)

  case class PredicatesAdded(iterationLength : Long, predicatesSelected : List[Predicate], initialClassSize: Int) extends UserEvent
  def logPredicateIter(iterTime: Long, predicatesSelected: List[Predicate], compEquivClassSize: Int) = writer.writeObject(
      PredicatesAdded(iterTime,predicatesSelected,compEquivClassSize))

  case class ProgramAccepted(iterationLength : Long, acceptedProgram: String, initialClassSize : Int) extends UserEvent
  def logAcceptIter(iterTime: Long, acceptedProgram : String, compEquivClassSize: Int) = writer.writeObject(
    ProgramAccepted(iterTime,acceptedProgram,compEquivClassSize))

  case class ExampleAdded(timeToCreate : Long) extends UserEvent
  def logExampleOk(exampleTime : Long) = writer.writeObject(ExampleAdded(exampleTime))

  case class ExampleCancelled(timeUntilCancel : Long) extends UserEvent
  def logExampleCancel(exampleTime : Long) = writer.writeObject(ExampleCancelled(exampleTime))

}

