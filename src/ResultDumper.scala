import java.io._

import IterInfo._

import scala.collection.mutable

/**
 * Created by hila on 11/04/2017.
 */
object ResultDumper extends App{
  val files = args.flatMap{dir => new java.io.File(dir).listFiles(new FilenameFilter {
    override def accept(dir: File, name: String): Boolean = name.endsWith(".session")
  })}

  val sessions = files.map { f =>
    val stream = new ObjectInputStream(new FileInputStream(f))
    val res = mutable.MutableList[UserEvent]()
    try {
      while (true) {
        val line = stream.readObject().asInstanceOf[UserEvent]
        res += line
      }
    }
    catch {
      case eof: EOFException => //this is fine
      case e: Exception => throw e
    }

    res.toList
  }

  val byExAndMode = sessions.groupBy{session => val start = session(0).asInstanceOf[SessionStarted]; (start.exercise.take(20), start.runMode)}
  byExAndMode.foreach{ be =>
    println(be._1)
    be._2.foreach{ session =>
      println(session.last)
    }
  }
}
