/**
 * Created by hila on 21/03/2017.
 */

import java.io.{EOFException, ObjectInputStream, FileInputStream}
import javax.script.ScriptException

import scala.collection.mutable
import scala.util.Try
import scala.util.matching.Regex

class Programs(val programs : Map[String,List[AnyRef]]) {
  def reExecute(): Programs = {
    val progsList = programs.keys.toList
    val programsStr: String = progsList.map(p => "Try(inputs.map(input => " + p + "))").mkString("List(", ",", ")")
    val results = Try(
      ScalaCompiler.engine.eval(programsStr).asInstanceOf[List[Try[List[_]]]]
    ).getOrElse(
      progsList.flatMap{ prog =>
        Try(
          Some(ScalaCompiler.engine.eval("Try(inputs.map(input => " + prog + "))").asInstanceOf[Try[List[_]]])
        ).getOrElse(None)
      }
    )
    val succeeded : Map[String,List[AnyRef]] = results.zip(progsList).flatMap{tp =>
      if (tp._1.isFailure) {
        None
      }
      else {//success
        Some((tp._2, tp._1.get.asInstanceOf[List[AnyRef]]))
      }
    }.toMap
    new Programs(succeeded)
  }


  def remove(program: (String, List[String])) = new Programs(programs.filter(p => p != program))

  def filter(predicates : List[Predicate]) : (Programs,Programs) = {
    val part = programs.partition(prog => predicates.forall(p => p.holds(prog._1, prog._2)))
    (new Programs(part._1),new Programs(part._2))
  }

  def containsProgram(program : String) : Boolean = programs.contains(program)
  def getResult(program : String) : List[AnyRef] = programs(program)
  def get = programs.head
  def isEmpty = programs.isEmpty

  def union(other : Programs)  = new Programs(programs ++ other.programs)

  def keepPrefixes(otherPrograms : Programs) : Programs = {
    val prefixes = for(program <- otherPrograms.programs.keys;
      subs = Programs.splitProgram(program);
      i <- 1 to subs.length;
      prog = subs.take(i).mkString("."))
      yield programs.get(prog).map(res => (prog, res))

    new Programs(prefixes.flatten.toMap)
  }
}

object Programs {
  def splitProgram(program : String) = new Regex("""(\([^\)]*\)|[^\.])+""").findAllIn(program).toList
  def apply(programsFile : String) = //new Programs(scala.io.Source.fromFile(programsFile).getLines()
    //.map(l => l.split("\t"))
    //.map(l => l(0) -> l.drop(1).take(1).toList)
    //.toMap)
  {
    val stream = new ObjectInputStream(new FileInputStream(programsFile))
    val res = mutable.MutableList[(String,AnyRef)]()
    try {
      while (true) {
        val line = stream.readObject().asInstanceOf[(String, AnyRef)]
        res += line
      }
    }
    catch{
      case eof : EOFException => //this is fine
      case e : Exception => throw e
    }
    new Programs(res.map(p => (p._1,List(p._2))).toMap)

  }
}
