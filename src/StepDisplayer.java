import scala.swing.Component;
import sun.swing.table.DefaultTableCellHeaderRenderer;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StepDisplayer extends JDialog {
    public boolean finished = false;

    public enum TestMode {
        syntax,
        io,
        fullGIM
    }
    private JPanel contentPane;
    private JTable table1;
    private JButton button1;
    private JList list1;
    private JTextArea instructionsText;
    private JPopupMenu rcMenu;
    private JMenuItem prefix;
    private JMenuItem exclude;
    private JMenuItem retain;
    private JMenuItem example;

    private final List<String> prog;
    public StepDisplayer(
            final List<String> program,
            final String [][] executions,
            TestMode mode,
            final String programString,
            final String inputString,
            String instructionString
    ) {
        this.prog = program;
        setContentPane(contentPane);
        setModal(true);

        this.instructionsText.setText(instructionString);
        final Action acceptAction = new AbstractAction("Accept program") {
            @Override
            public void actionPerformed(ActionEvent e) {
                finished = true;
                closeWindow();
            }
        };
        final Action applyAction = new AbstractAction("Apply answers") {
            @Override
            public void actionPerformed(ActionEvent e) {
                finished = false;
                closeWindow();
            }
        };
        button1.setAction(acceptAction);

        List<String> cols = new ArrayList<>(executions[0].length + 1);
        cols.add("code");
        for (int i = 1; i <= executions[0].length; ++i) {
            cols.add("Debug information (example " + i + ")");
        }
        String[][] rows = new String[executions.length][];
        for (int i = 0; i < executions.length; ++i) {
            String [] row = new String[executions[i].length + 1];
            row[0] = program.get(i);
            for (int j = 0; j < executions[i].length; ++j) {
                row[j+1] = executions[i][j];
            }
            rows[i] = row;
        }
        final DefaultTableModel model = new DefaultTableModel(rows, cols.toArray());
        table1.setModel(new TableModel() {

            @Override
            public int getRowCount() {
                return model.getRowCount();
            }

            @Override
            public int getColumnCount() {
                return model.getColumnCount();
            }

            @Override
            public String getColumnName(int columnIndex) {
                return model.getColumnName(columnIndex);
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return model.getColumnClass(columnIndex);
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                return model.getValueAt(rowIndex,columnIndex);
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

            }

            @Override
            public void addTableModelListener(TableModelListener l) {
                model.addTableModelListener(l);
            }

            @Override
            public void removeTableModelListener(TableModelListener l) {
                model.removeTableModelListener(l);
            }
        });
        //table1.getTableHeader().setVisible(true);
        final DefaultListModel lmodel = new DefaultListModel();

        rcMenu = new JPopupMenu();

        prefix = new JMenuItem("Affix prefix");
        prefix.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int [] selected = table1.getSelectedRows();
                Arrays.sort(selected);
                predicates.add(new PrefixPredicate(program.subList(selected[0],selected[selected.length - 1]+1).toArray(new String[] {})));
                lmodel.addElement(predicates.get(predicates.size() - 1));
                button1.setAction(applyAction);
            }
        });
        exclude = new JMenuItem("Exclude sequence");
        exclude.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int [] selected = table1.getSelectedRows();
                Arrays.sort(selected);
                predicates.add(new ExcludePredicate(program.subList(selected[0],selected[selected.length - 1]+1).toArray(new String[] {})));
                lmodel.addElement(predicates.get(predicates.size() - 1));
                button1.setAction(applyAction);
            }
        });
        retain  = new JMenuItem("Retain sequence");
        retain.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int [] selected = table1.getSelectedRows();
                Arrays.sort(selected);
                predicates.add(new RetainPredicate(program.subList(selected[0],selected[selected.length - 1]+1).toArray(new String[] {})));
                lmodel.addElement(predicates.get(predicates.size() - 1));
                button1.setAction(applyAction);
            }
        });
        example = new JMenuItem("Add I/O Example");
        example.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                InputOutputDialog iod = new InputOutputDialog();
                iod.pack();
                iod.setVisible(true);
                if (iod.isOk) {
                    predicates.add(new OutputEqualsPredicate(iod.input.getText(),iod.output.getText()));
                    lmodel.addElement(predicates.get(predicates.size() - 1));
                    button1.setAction(applyAction);
                }
            }
        });

        exclude.setVisible(false);
        retain.setVisible(false);
        prefix.setVisible(false);

        if (mode != TestMode.io) {
            rcMenu.add(prefix);
            rcMenu.add(exclude);
            rcMenu.add(retain);
        }
        if (mode != TestMode.syntax) {
            rcMenu.add(example);
        }

        rcMenu.add(new AbstractAction("Copy program to clipboard") {

            @Override
            public void actionPerformed(ActionEvent e) {
                StringSelection selection = new StringSelection(programString);
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(selection,selection);
            }
        });

        rcMenu.add(new AbstractAction("Copy program and inputs to clipboard") {

            @Override
            public void actionPerformed(ActionEvent e) {
                StringBuilder sb = new StringBuilder();
                sb.append("val inputs = ");
                sb.append(inputString);
                sb.append("\n");
                sb.append("inputs.map(input => ");
                sb.append(programString);
                sb.append(")");
                StringSelection selection = new StringSelection(sb.toString());
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(selection,selection);
            }
        });
        table1.setComponentPopupMenu(rcMenu);
        contentPane.setComponentPopupMenu(rcMenu);
        table1.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int [] selected = table1.getSelectedRows();
                Arrays.sort(selected);
                boolean isSequential = selected.length != 0 && selected[selected.length - 1] - selected[0] + 1 == selected.length;
                if (isSequential && !(selected.length == 1 && selected[0] == 0)) {
                    exclude.setVisible(true);
                    retain.setVisible(true);
                    prefix.setVisible(true);
                    if (selected[0] > 0) {
                        prefix.setVisible(false);
                    }
                }
                else {
                    exclude.setVisible(false);
                    retain.setVisible(false);
                    prefix.setVisible(false);
                }
            }
        });


        list1.setModel(lmodel);

        for(int i = 0; i < table1.getColumnModel().getColumnCount(); ++i) {
            computeColumnWidth(table1.getColumnModel().getColumn(i),i);
        }

        ((DefaultTableCellRenderer) table1.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.LEFT);

        JPopupMenu listMenu = new JPopupMenu();

        final AbstractAction removeFromList = new AbstractAction("Remove predicate") {
            @Override
            public void actionPerformed(ActionEvent e) {
                int [] idxes = list1.getSelectedIndices();
                Arrays.sort(idxes);
                for (int i = idxes.length - 1; i >= 0; --i) {
                    int idx = idxes[i];
                    lmodel.remove(idx);
                    predicates.remove(idx);
                }
                if (lmodel.isEmpty()) {
                    button1.setAction(acceptAction);
                }
            }
        };
        removeFromList.setEnabled(false);
        listMenu.add(removeFromList);

        list1.setComponentPopupMenu(listMenu);
        list1.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(list1.getSelectedIndices().length == 0) {
                    removeFromList.setEnabled(false);
                }
                else {
                    removeFromList.setEnabled(true);
                }
            }
        });
    }

    private void computeColumnWidth(TableColumn col, int column) {
        int width = 0;
        for (int r = 0; r < table1.getRowCount(); r++) {
            TableCellRenderer renderer = table1.getCellRenderer(r, column);
            java.awt.Component comp = renderer.getTableCellRendererComponent(table1, table1.getValueAt(r, column), false, false, r, column);
            int currentWidth = comp.getPreferredSize().width;
            width = Math.max(width, currentWidth);
        }
        width += 4;
        width = Math.max(width,120);
        col.setPreferredWidth(width);
        col.setWidth(width);
    }

    public void closeWindow() {
        this.setVisible(false);
    }

    public List<Predicate> predicates = new ArrayList<>();

//    public static void main(String[] args) {
//        StepDisplayer dialog = new StepDisplayer();
//        dialog.pack();
//        dialog.setVisible(true);
//        System.exit(0);
//    }
}
