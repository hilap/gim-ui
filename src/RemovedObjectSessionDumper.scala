import java.io._

import IterInfo.{ProgramAccepted, PredicatesAdded, SessionStarted, UserEvent}

import scala.collection.mutable

/**
 * Created by hila on 30/07/2017.
 */
object RemovedObjectSessionDumper extends App{
  val filesOfEx = args.drop(6).flatMap{dir => new java.io.File(dir).listFiles(new FilenameFilter {
    override def accept(dir: File, name: String): Boolean = name.endsWith(".session") && name.startsWith("io")
  })}
  val filesOthers = args.drop(6).flatMap{dir => new java.io.File(dir).listFiles(new FilenameFilter {
    override def accept(dir: File, name: String): Boolean = name.endsWith(".session") && !name.startsWith("io")
  })}

  val sessionsOfEx = getSessions(filesOfEx).groupBy(session => session(0).asInstanceOf[SessionStarted].exercise.take(20))
  val sessionsOfOthers = getSessions(filesOthers).groupBy(session => session(0).asInstanceOf[SessionStarted].exercise.take(20))


  val programFiles = Map("Count the number of " -> Array(args(0),args(3)),
    "Find the most freque" -> Array(args(1),args(4)),
    "Create a histogram o" -> Array(args(2),args(5)))

  val removedSeqs = sessionsOfOthers.map(group =>
      group._1 ->
      group._2.flatMap(step => step).filter(step => step.isInstanceOf[PredicatesAdded]).map(step => step.asInstanceOf[PredicatesAdded])
              .flatMap(step => step.predicatesSelected).filter(pred => pred.isInstanceOf[ExcludePredicate])
              .map(pred => pred.asInstanceOf[ExcludePredicate].seq).groupBy(arr => arr.mkString(".")).map(_._2(0)).toSet)

  val removedPerGroup = sessionsOfOthers.map{group =>
    group._1 -> removedSeqs(group._1).map{sequence => sequence ->
      group._2.map(session => session.filter(step => step.isInstanceOf[PredicatesAdded]).flatMap(step => step.asInstanceOf[PredicatesAdded].predicatesSelected).filter(pred => pred.isInstanceOf[ExcludePredicate]).map(pred => pred.asInstanceOf[ExcludePredicate].seq.mkString("."))).count(session => session.contains(sequence.mkString(".")))
    }.filter(_._2 > 2).toList.sortBy(-_._2)
  }

  //println(removedPerGroup.map(kv => kv._1 -> kv._2.map(kv2 => kv2._1.mkString(".") -> kv2._2)))

  sessionsOfEx.foreach{ group =>
    println()
    println(group._1)
    println("#sessions in !io: " + sessionsOfOthers(group._1).length)
    println(removedPerGroup(group._1).map(kv2 => kv2._1.mkString(".") -> kv2._2))
    val countAll = mutable.Map[String,Int]().withDefaultValue(0);
    val sumAll = mutable.Map[String,Int]().withDefaultValue(0);
    val moreThanOneSum = mutable.Map[String,Int]().withDefaultValue(0);
    val moreThanOneCount = mutable.Map[String,Int]().withDefaultValue(0);
    val maxAmt = mutable.Map[String,Int]().withDefaultValue(0);
    val minAmt = mutable.Map[String,Int]();
    group._2.foreach{ session =>
      val replay = replaySession(session,programFiles(group._1)(1),programFiles(group._1)(0))
      val replaySplit = replay.map(program => Programs.splitProgram(program))
      removedPerGroup(group._1).foreach{removed =>
        val appearances = replaySplit.count{program =>
          (1 to program.length-removed._1.length+1).exists(i =>
            program.drop(i).take(removed._1.length).sameElements(removed._1))
        }
        val asString = removed._1.mkString(".")
        if (appearances  > 1) {
          moreThanOneCount(asString) += 1
          moreThanOneSum(asString) += appearances
        }
        if (appearances > 0) {
          countAll(asString) += 1
        }
        sumAll(asString) += appearances
        maxAmt(asString) = Math.max(maxAmt(asString),appearances)
        minAmt(asString) = if (minAmt.contains(asString)) Math.min(minAmt(asString),appearances) else appearances

      }
    }

    val replayedOthers = sessionsOfOthers(group._1).map( session =>
        replaySession(session,programFiles(group._1)(1),programFiles(group._1)(0)).map(Programs.splitProgram(_))
      )

    countAll.foreach{ kv =>
      print(kv._1)
      val splitExcluded = Programs.splitProgram(kv._1)
      print("\t")
      val perc = removedPerGroup(group._1).find(_._1.mkString(".") == kv._1).get._2.toDouble * 100 /
        replayedOthers.indices.count(j =>
          sessionsOfOthers(group._1)(0).exists(event => event.isInstanceOf[PredicatesAdded] && event.asInstanceOf[PredicatesAdded].predicatesSelected.exists(pred => pred.isInstanceOf[ExcludePredicate] && pred.asInstanceOf[ExcludePredicate].seq.mkString(".") == kv._1)) ||
          replayedOthers(j).exists(prog => (1 to prog.length - splitExcluded.length).exists(i =>
            prog.drop(i).take(splitExcluded.length).mkString(".") == kv._1))
        )
      /*sessionsOfOthers(group._1).size*/

      print(f"$perc%1.1f")
      print("% (")
      print(removedPerGroup(group._1).find(_._1.mkString(".") == kv._1).get._2)
      print(")")
      print("\t")
      print(minAmt(kv._1))
      print("\t")
      print(maxAmt(kv._1))
      print("\t")
      //print(kv._2)
      //print("\t")
      //print(sumAll(kv._1))
      //print("\t")
      var avg = moreThanOneSum(kv._1).toDouble / moreThanOneCount(kv._1)
      if (avg.isNaN) avg = 0

      print(f"$avg%1.1f")
      print("\t")
      val prc2 = moreThanOneCount(kv._1).toDouble * 100 / sessionsOfEx(group._1).size
      print(f"$prc2%1.1f")
      print("% (")
      print(moreThanOneCount(kv._1))
      println(")")
    }
    //println(moreThanOne)
    //println(sumAll)
    //println(countAll)

  }

  def replaySession(session : List[UserEvent], ioFile : String, programsFile : String) : List[String] = {
    ScalaCompiler.reset()
    OutputEqualsPredicate.zeroIdx

    var (in,out) = Main.loadInputOutputs(ioFile)
    var inputs = ScalaCompiler.setInputs(in)

    var (programs,_) = Main.loadInitialPrograms(in,out,programsFile)

    val res = mutable.MutableList[String](programs.get._1)

    val events = if (session.exists(event => event == IterInfo.ProgramsEmptied)) session.drop(session.lastIndexWhere(event => event == IterInfo.ProgramsEmptied)) else session
    events.filter(event => event.isInstanceOf[PredicatesAdded]).map(_.asInstanceOf[PredicatesAdded]).foreach {
      predicates =>
        //assert(predicates.predicatesSelected.forall(_.isInstanceOf[OutputEqualsPredicate]));
        val notExamples = predicates.predicatesSelected.filter(!_.isInstanceOf[OutputEqualsPredicate])
        //apply nonexamples
        val (g,b) = programs.filter(notExamples)
        programs = g
        val examples = predicates.predicatesSelected.filter(_.isInstanceOf[OutputEqualsPredicate]).map(_.asInstanceOf[OutputEqualsPredicate])
        if (!examples.isEmpty) {
          examples.foreach(ex => ex.setIdx)
          in ++= examples.map(_.in)
          inputs = ScalaCompiler.setInputs(in)
          programs = programs.reExecute()
          val (good, bad) = programs.filter(examples)
          programs = good
        }
        if (!programs.isEmpty)
          res += programs.get._1
    }

    //assert(!session.last.isInstanceOf[ProgramAccepted] || session.last.asInstanceOf[ProgramAccepted].acceptedProgram == res.last)
    res.toList
  }


  def getSessions(files : Array[File]): Array[List[UserEvent]] = {
    files.map { f =>
      val stream = new ObjectInputStream(new FileInputStream(f))
      val res = mutable.MutableList[UserEvent]()
      try {
        while (true) {
          val line = stream.readObject().asInstanceOf[UserEvent]
          res += line
        }
      }
      catch {
        case eof: EOFException => //this is fine
        case e: Exception => throw e
      }

      res.toList
    }
  }
}
